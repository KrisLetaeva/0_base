$(".btn-reveal").click(function(event) {
	event.preventDefault();
	var href = $(this).attr('href');
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$(this).text($(this).attr('data-text'));
		$(".hidden-content"+href).slideUp().removeClass('active');
	} else { 
		$(this).addClass('active');
		$(this).text($(this).attr('data-text-active'));
		$(".hidden-content"+href).slideDown().addClass('active');
	}
});