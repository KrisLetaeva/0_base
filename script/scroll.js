jQuery('html, body').hide();
jQuery(document).ready(function($) {

	// $(document).on('click', 'a[href*=#]', jump);

    if (location.hash){
        setTimeout(function(){
            $('html, body').scrollTop(0).show();
            jump();
        }, 0);
    }else{
        $('html, body').show();
	}
	
	wow = new WOW(
		{
			mobile: false
		}
	)
	wow.init();

	initParalaxBg();

	new ScrollFlow();

	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = 60;
	$(window).scroll(function () {
		didScroll = true;
	});

	setInterval(function () {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = $(this).scrollTop();

		// Make sure they scroll more than delta
		if (Math.abs(lastScrollTop - st) <= delta)
			return;

		// If they scrolled down and are past the navbar, add class .nav-up.
		// This is necessary so you never see what is "behind" the navbar.
		if (st > lastScrollTop && st > navbarHeight) {
			// Scroll Down
			$('.header').removeClass('nav-down').addClass('nav-up');
		} else {
			// Scroll Up
			if (st + $(window).height() < $(document).height()) {
				$('.header').removeClass('nav-up').addClass('nav-down');
			}
		}
		lastScrollTop = st;
	}

	$('body').on('click',".jump-to",function(event) {
		event.preventDefault();
		var $anchor = $(this).attr("href");
		$('html, body').stop().animate({scrollTop: $($anchor).offset().top}, 1000);
	});

	/*$('body').on('click','.logo',function () {
		event.preventDefault();
		$('body,html').animate({
		scrollTop: 0
		}, 800);
		return false;
	});*/
	
});

var jump = function(e)
{
    //alert('here');
   if (e){
       //e.preventDefault();
       var target = jQuery(this).attr("href").replace('/', '');
   }else{
       var target = location.hash;
   }

   jQuery('html,body').animate(
   {
       scrollTop: (jQuery(target).offset().top) - 100
   },500,function()
   {
       //location.hash = target;
   });

}