'use strict';

var fs = require('fs');
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');

var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

var pug = require('gulp-pug');
var plumber = require('gulp-plumber');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

//var imagemin = require('gulp-imagemin');
var image = require('gulp-image');

// compile html
gulp.task('html', function(){
	return gulp.src('./index.html')
	.pipe(gulp.dest('./public/'))
	.pipe(browserSync.stream());
});

// compile scss
gulp.task('scss', function(){
	return gulp.src('./scss/style.scss')
	.pipe(sass({includePaths: ['./scss/*.scss','./scss/**/*.scss']}).on('error', sass.logError))
	.pipe(autoprefixer('last 2 versions'))
	//.pipe(cleanCSS())
	.pipe(gulp.dest('./public/css/'))
	.pipe(browserSync.stream());
});

// compile pug (jade) templates  
gulp.task('pug', function(){
	return gulp.src('./pug/*.pug')
	.pipe(plumber())
	.pipe(pug({
		pretty: '\t'
	}))
	.pipe(gulp.dest('./public/'))
	.pipe(browserSync.stream());
});

// task concat dependencies from package.json
gulp.task('vendor', function(){
	
	var json = JSON.parse(fs.readFileSync('./package.json'));
	var vendor = [];
	var deps = json.dependencies;

	for (var lib in deps) { 
		// console.log(lib);
		var ver = deps[lib].replace('^','');
		var libInner = lib.replace('-dist','');
		var varies = [
			'./node_modules/'+lib+'/dist/'+lib+'.js', 
			'./node_modules/'+lib+'/dist/jquery.'+lib+'.js', 
			'./node_modules/'+lib+'/'+lib+'.js',
			'./node_modules/'+lib+'/'+libInner+'.js' 
		];
		var found = false;
		varies.forEach(function(path) {
			if(found==false) {
				if(fs.existsSync(path)) {
					vendor.push(path);
					// console.log(path);
					// console.log('---');
					found = true;
				}
			}
		});
	}
		
	return gulp.src('script/vendor/*.js')
	//.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(concat('vendor.js'))
	// .pipe(uglify())
	// .pipe(sourcemaps.write('maps'))
	.pipe(gulp.dest('./public/js/vendor/'));
});

// task to concat custom js from ./script 
// enviroment with uglify in TODO
gulp.task('js', function(){
		
	//gulp.src('script/*.js').pipe(gulp.dest('./public/js/'));
	return gulp.src('script/*.js')
	.pipe(concat('app.js'))
	//.pipe(uglify())
	.pipe(gulp.dest('./public/js/'))
	.pipe(browserSync.stream());
});

gulp.task('fonts', function(){
	return gulp.src('fonts/*.*')
	.pipe(gulp.dest('./public/fonts/'))
	.pipe(browserSync.stream());
});

gulp.task('fav', function(){
	return gulp.src('fav/*.*')
	.pipe(gulp.dest('./public/fav/'))
	.pipe(browserSync.stream());
});


gulp.task('images', function(){
	return gulp.src('images/*')
	.pipe(image())
	.pipe(gulp.dest('./public/images/'))
});

gulp.task('build', ['vendor', 'scss', 'js', 'pug', 'fonts', 'fav', 'images', 'html'], function(){
	console.log('------------------');
	console.log('- build finished -');
	console.log('------------------');
	return true;
}); 

gulp.task('default',[ 'html', 'scss', 'vendor', 'js', 'pug'], function(){

	browserSync.init({
        server: {
		    baseDir: "public",
		    directory: true
		},
        port: 8080,
        ui: {
		    port: 8081
		}

    });


	gulp.watch([
		'./scss/*.scss',
		'./scss/**/*.scss'
		]
		, ['scss']);

	gulp.watch([
		'./pug/*.pug',
		'./pug/**/*.pug'
		]
		, ['pug']);

	gulp.watch([
		'*.html'
		]
		, ['html']);

	gulp.watch([
		'./script/*.js',
		'./script/vendor/*.js',
		]
		, ['js']);
}); 
